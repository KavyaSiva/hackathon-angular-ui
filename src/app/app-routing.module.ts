import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockEditComponent } from './stock-edit/stock-edit.component';
import { StockListComponent } from './stock-list/stock-list.component';
import {GetstocksComponent} from './getstocks/getstocks.component';
import {StockTableFunctionsComponent} from './stock-table-functions/stock-table-functions.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stock-list' },
  { path: 'create-stock', component: StockCreateComponent },
  { path: 'stock-list', component: StockListComponent },
  { path: 'stock-edit/:id', component: StockEditComponent }  ,
  {path: 'getallstocks' , component: GetstocksComponent},
  {path:'stockTable', component: StockTableFunctionsComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
