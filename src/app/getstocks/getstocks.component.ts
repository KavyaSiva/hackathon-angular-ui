import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-getstocks',
  templateUrl: './getstocks.component.html',
  styleUrls: ['./getstocks.component.css']
})

export class GetstocksComponent implements OnInit {
  ticker:any=[];
  selected:any =[];
  Stocklist: any = [];
  price: number = 0;

 public name : any;
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }
//.subscribe(res => this.items = res.json(),
  loadStocks() {
    return this.restApi.getStockList().subscribe((data: {}) => {
        this.Stocklist = data;
    })
  }

  selectedChanged() {
    console.log(this.selected);
  }
 /* getPrice(){
    return this.restApi.getPrice(this.ticker).subscribe((data: {}) => {
      this.Stocklist = data;
  })
    console.log("function called");
  }
*/
  getStockPrice(){
    return this.restApi.getPrice(this.selected).subscribe((data: {}) => {
      this.ticker = data;
      console.log(data);
     // console.log((data.price_data[0])[1]);
     console.log((this.ticker[Object.keys(this.ticker)[1]])[0][1]);
     this.price = (this.ticker[Object.keys(this.ticker)[1]])[0][1];
    }
    
    )
  }

  changeLabelName() {
    this.name = this.selected;
  }

  putPrice(){
    
  }
}

/*
export class GetstocksComponent {
  constructor(private http:Http) {
    this.http.get('https://jsonplaceholder.typicode.com/todos')
      .subscribe(res => this.items = res.json(),
    err => console.error('Error: ' + err),
    () => console.log('Voila! you got your list!'));
  }
}
*/
